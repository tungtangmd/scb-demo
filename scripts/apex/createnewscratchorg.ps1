Write-Host "Create new scratch Org" -ForegroundColor green
$targetOrg = Read-Host -Prompt 'Enter New Org Alias'
$sourceOrg = Read-Host -Prompt 'Enter Source Org from where to copy test data'

Write-Host "Creating new scratch org $targetOrg" -ForegroundColor yellow
sfdx force:org:create -f config/project-scratch-def.json -d 30 -a $targetOrg -s -w 30
Write-Host "Finished creating new scratch org $targetOrg" -ForegroundColor green

Write-Host "Installing FSC Package" -ForegroundColor yellow
sfdx force:package:install --package="04t1E000000jazHQAQ" -u $targetOrg -w 30
Write-Host "Finished installing FSC Package" -ForegroundColor green

Write-Host "Installing FSC Package Extension" -ForegroundColor yellow
sfdx force:package:install --package="04t1E000001Iql5" -u $targetOrg -w 30
Write-Host "Finished installing FSC Package Extension" -ForegroundColor green

Write-Host "Deploying source to $targetOrg" -ForegroundColor yellow
sfdx force:source:push -u $targetOrg -f
Write-Host "Finished deploying source to $targetOrg" -ForegroundColor green

Write-Host "Adding permission to the user" -ForegroundColor yellow
sfdx force:apex:execute -f ./scripts/apex/initialusersetup.apex -u $targetOrg
Write-Host "Finished adding permission to the user" -ForegroundColor green

Write-Host "Copying test data from $sourceOrg to $targetOrg" -ForegroundColor yellow
sfdx sfdmu:run -s $sourceOrg -u $targetOrg -p ./scripts/sfdmu/conf
Write-Host "Finished copying test data from $sourceOrg to $targetOrg" -ForegroundColor green

Write-Host "Deactivating default RBL" -ForegroundColor yellow
sfdx force:apex:execute -u $targetOrg -f ./scripts/apex/deactivateRBL.apex
Write-Host "Finished deactivating default RBL" -ForegroundColor green

Write-Host "Importing new RBL rules" -ForegroundColor yellow
sfdx force:data:tree:import -f ./scripts/data/FinServ__RollupByLookupConfig__c-FinServ__RollupByLookupFilterCriteria__c.json -u $targetOrg
Write-Host "Finished importing new RBL rules" -ForegroundColor green

Write-Host "Activating new imported RBL rules" -ForegroundColor yellow
sfdx force:apex:execute -u $targetOrg -f ./scripts/apex/activateSCBRBL.apex
Write-Host "Finished activating new imported RBL rules" -ForegroundColor green

Write-Host "Deleting packaged list views" -ForegroundColor yellow
sfdx force:mdapi:deploy -d destroyPackage\postDestroy -u $targetOrg -w -1
Write-Host "Finished deleting packaged list views" -ForegroundColor green

Write-Host "Opening $targetOrg in browser" -ForegroundColor green
sfdx force:org:open -u $targetOrg
