public with sharing class SCB_AccountTriggerClass {
    public static boolean isBeforeInsertExecuted = false;
    public static boolean isBeforeUpdateExecuted = false;
    public static boolean isBeforeDeleteExecuted = false;
    public static boolean isAfterInsertExecuted = false;
    public static boolean isAfterUpdateExecuted = false;
    public static boolean isAfterDeleteExecuted = false;
    public static boolean isAfterUndeleteExecuted = false;
    
    // public static void clear_execution_flags(){
    // isBeforeInsertExecuted = false;
    // isBeforeUpdateExecuted = false;
    // isBeforeDeleteExecuted = false;
    // isAfterInsertExecuted = false;
    // isAfterUpdateExecuted = false;
    // isAfterDeleteExecuted = false;
    // isAfterUndeleteExecuted = false;
    // }
    
    /* Method name: executeTriggerEvents
     * Purpose: This method checks the trigger events and accordingly execute the 
     *          methods  along with the trigger context variables and data
     */
    public static void executeTriggerEvents(Boolean isBefore, Boolean isAfter, 
                                            Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, 
                                            Map<Id, Account> oldMap, List<Account> newList, Map<Id, Account> newMap){
        // if(isBefore){
        //     if(isInsert){
        //        if(!isBeforeInsertExecuted){
		// 			isBeforeInsertExecuted = true;
        //        }
        //     }
            
        //     if(isUpdate){
        //        if(!isBeforeUpdateExecuted){
        //             isBeforeUpdateExecuted = true;
        //        }
        //     }
            
        //     if(isDelete){
        //        if(!isBeforeDeleteExecuted){
        //            isBeforeDeleteExecuted = true;
        //        }
        //     }
        // }
        
        if(isAfter){
            if(isInsert){
			    if(!isAfterInsertExecuted){
                    updateRelationshipNo(newList, new Map<Id, Account>(), new Map<Id, Account>());
                    if(!Test.isRunningTest()){
                        isAfterInsertExecuted = true;
                    }
                }
            }
            
            if(isUpdate){
			    if(!isAfterUpdateExecuted){
                    updateRelationshipNo(new List<Account>(), oldMap, newMap);
                    if(!Test.isRunningTest()){
                        isAfterUpdateExecuted = true;
                    }
                }
            }
            
            // if(isDelete){
            //     if(!isAfterDeleteExecuted){
            //         isAfterDeleteExecuted = true;
            //     }
            // }
            
            // if(isUndelete){
            //     if(!isAfterUndeleteExecuted){
            //         isAfterUndeleteExecuted = true;
            //     }
            // }
        }
    }

    public static void updateRelationshipNo(List<Account> newList, Map<Id, Account> oldMap, Map<Id, Account> newMap){
        Map<Id, String> contactMap = new Map<Id, String>();

        for(Account acc: newList){
            if(acc.IsPersonAccount && 
               String.isNotBlank(acc.Relationship_No__c)){
                contactMap.put(acc.PersonContactId , acc.Relationship_No__c);
            }
        }
        
        for(Id accId: oldMap.keySet()){
            if(oldMap.get(accId).IsPersonAccount && 
               oldMap.get(accId).Relationship_No__c != newMap.get(accId).Relationship_No__c){
                contactMap.put(newMap.get(accId).PersonContactId, newMap.get(accId).Relationship_No__c);
            }
        }

        if(!contactMap.isEmpty()){
            List<Contact> contactList = new List<Contact>();
            
            for(Id contactId: contactMap.keySet()){
                Contact con = new Contact();
                con.Id = contactId;
                con.Relationship_No__c = contactMap.get(contactId);
                contactList.add(con);
            }

            List<Database.SaveResult> srList = Database.update(contactList, false);
            SCB_ApexErrorLogUtility.logDMLResult('SCB_AccountTrigger', 'SCB_AccountTriggerClass', 'updateRelationshipNo', srList);
        }
    }
}