public class SCB_ClientAumMixController {
    
    @AuraEnabled(cacheable=true)
    public static List<DataSet> getFinancialAccountsMix(Id accountId){
        Account client = SCB_QueryWithoutSharing.getClientDetails(accountId);
        
        List<DataSet> dataSet = new List<DataSet>();
        dataSet.add(new DataSet(SCB_Constants.SEGMENT_TYPE_WEALTH_MANAGEMENT, client.Total_Wealth_Management_Primary_Owner__c));
        dataSet.add(new DataSet(SCB_Constants.SEGMENT_TYPE_DEPOSITS, client.Total_Deposits_Primary_Owner__c));
        dataSet.add(new DataSet(SCB_Constants.SEGMENT_TYPE_BANCA, client.Total_Banca_Primary_Owner__c != null ? client.Total_Banca_Primary_Owner__c : 0));
        dataSet.add(new DataSet(SCB_Constants.SEGMENT_TYPE_LOANS, client.Total_Loans_Primary_Owner__c));
        return dataSet;
    }
    
    @AuraEnabled(cacheable=true)
    public static Map<String, Map<String, Decimal>> getFinancialAccountsTrend(Id accountId, Integer trend, boolean isWM){
        Map<String, Set<String>> accountSegmentMap = getAccountSegment();
        Set<String> recordTypeNameSet = new Set<String>();
        
        for(String segment: accountSegmentMap.keySet()){
            for(String accountType: accountSegmentMap.get(segment)){
                if(isWM){
                    if(segment == SCB_Constants.SEGMENT_TYPE_WEALTH_MANAGEMENT){
                        recordTypeNameSet.add(accountType);
                    }
                } else {
                    recordTypeNameSet.add(accountType);
                }
            }
        }
 
        Map<Integer, String> monthMap = new Map<Integer, String>();
        monthMap.put(1, 'JAN');
        monthMap.put(2, 'FEB');
        monthMap.put(3, 'MAR');
        monthMap.put(4, 'APR');
        monthMap.put(5, 'MAY');
        monthMap.put(6, 'JUN');
        monthMap.put(7, 'JUL');
        monthMap.put(8, 'AUG');
        monthMap.put(9, 'SEP');
        monthMap.put(10, 'OCT');
        monthMap.put(11, 'NOV');
        monthMap.put(12, 'DEC');

        Date monthNow = Date.today();
        Date monthPast = Date.today().addMonths(-trend);
        List<Integer> monthInterval = new List<Integer>();
        List<Integer> monthIntervalReversed = new List<Integer>();

        while(monthNow > monthPast){
            monthInterval.add(monthNow.Month());
            monthNow = monthNow.addMonths(-1);
        }

        for(Integer i = monthInterval.size() - 1; i >= 0; i--){
            monthIntervalReversed.add(monthInterval.get(i));
        }

        Map<String, Map<String, List<Decimal>>> segmentMap = new Map<String,Map<String,List<Decimal>>>();
        
        for(AggregateResult ar: SCB_QueryWithoutSharing.getAggregatedFinancialAccounts(accountId, recordTypeNameSet)){

            for(String segment: accountSegmentMap.keySet()){
                List<Decimal> tempList = new List<Decimal>();

                tempList.add((Decimal)ar.get('sum'));
                tempList.add((Decimal)ar.get('balanceM1'));
                tempList.add((Decimal)ar.get('balanceM2'));
                tempList.add((Decimal)ar.get('balanceM3'));
                tempList.add((Decimal)ar.get('balanceM4'));
                tempList.add((Decimal)ar.get('balanceM5'));
                
                if(trend == 12){
                    tempList.add((Decimal)ar.get('balanceM6'));
                    tempList.add((Decimal)ar.get('balanceM7'));
                    tempList.add((Decimal)ar.get('balanceM8'));
                    tempList.add((Decimal)ar.get('balanceM9'));
                    tempList.add((Decimal)ar.get('balanceM10'));
                    tempList.add((Decimal)ar.get('balanceM11'));
                    tempList.add((Decimal)ar.get('balanceM12'));
                }
                
                if(accountSegmentMap.get(segment).contains((String)ar.get('DeveloperName'))){
                    if(!segmentMap.containsKey(segment)){
                        Map<String, List<Decimal>> tempMap = new Map<String, List<Decimal>>();
                        tempMap.put((String)ar.get('DeveloperName'), tempList);
                        segmentMap.put(segment, tempMap);
                    } else {
                        segmentMap.get(segment).put((String)ar.get('DeveloperName'), tempList);
                    }
                }
            }
        }

        Map<String, List<Decimal>> segmentTotalMap = new Map<String, List<Decimal>>();

        for(String segment: segmentMap.keySet()){

            Decimal monthOne = 0;
            Decimal monthTwo = 0;
            Decimal monthThree = 0;
            Decimal monthFour = 0;
            Decimal monthFive = 0;
            Decimal monthSix = 0;
            Decimal monthSeven = 0;
            Decimal monthEight = 0;
            Decimal monthNine = 0;
            Decimal monthTen = 0;
            Decimal monthEleven = 0;
            Decimal monthTwelve = 0;

            for(String accountType: segmentMap.get(segment).keySet()){

                monthOne += segmentMap.get(segment).get(accountType)[0] != null ? segmentMap.get(segment).get(accountType)[0] : 0;
                monthTwo += segmentMap.get(segment).get(accountType)[1] != null ? segmentMap.get(segment).get(accountType)[1] : 0;
                monthThree += segmentMap.get(segment).get(accountType)[2] != null ? segmentMap.get(segment).get(accountType)[2] : 0;
                monthFour += segmentMap.get(segment).get(accountType)[3] != null ? segmentMap.get(segment).get(accountType)[3] : 0;
                monthFive += segmentMap.get(segment).get(accountType)[4] != null ? segmentMap.get(segment).get(accountType)[4] : 0;
                monthSix += segmentMap.get(segment).get(accountType)[5] != null ? segmentMap.get(segment).get(accountType)[5] : 0;
                
                if(trend == 12){
                    monthSeven += segmentMap.get(segment).get(accountType)[6] != null ? segmentMap.get(segment).get(accountType)[6] : 0;
                    monthEight += segmentMap.get(segment).get(accountType)[7] != null ? segmentMap.get(segment).get(accountType)[7] : 0;
                    monthNine += segmentMap.get(segment).get(accountType)[8] != null ? segmentMap.get(segment).get(accountType)[8] : 0;
                    monthTen += segmentMap.get(segment).get(accountType)[9] != null ? segmentMap.get(segment).get(accountType)[9] : 0;
                    monthEleven += segmentMap.get(segment).get(accountType)[10] != null ? segmentMap.get(segment).get(accountType)[10] : 0;
                    monthTwelve += segmentMap.get(segment).get(accountType)[11] != null ? segmentMap.get(segment).get(accountType)[11] : 0;
                }
            }
            
            List<Decimal> tempList = new List<Decimal>();
            
            if(trend == 12){
                tempList.add(monthTwelve);
                tempList.add(monthEleven);
                tempList.add(monthTen);
                tempList.add(monthNine);
                tempList.add(monthEight);
                tempList.add(monthSeven);
            }
            
            tempList.add(monthSix);
            tempList.add(monthFive);
            tempList.add(monthFour);
            tempList.add(monthThree);
            tempList.add(monthTwo);
            tempList.add(monthOne);

            if(isWM){
                for(String accountType: segmentMap.get(segment).keySet()){
                    segmentTotalMap.put(accountType, tempList);
                }
            } else {
                segmentTotalMap.put(segment, tempList);
            }
        }

        Map<String, Map<String, Decimal>> orderedMonthlySegmentTotal = new Map<String, Map<String, Decimal>>();
        
        for(String segment: segmentTotalMap.keySet()){
            Integer counter = 0;
            for(Decimal total: segmentTotalMap.get(segment)){
                if(!orderedMonthlySegmentTotal.keySet().contains(monthMap.get(monthIntervalReversed[counter]))){
                    Map<String, Decimal> tempMap = new Map<String, Decimal>();
                    tempMap.put(segment, segmentTotalMap.get(segment)[counter]);
                    orderedMonthlySegmentTotal.put(monthMap.get(monthIntervalReversed[counter]), tempMap);
                } else {
                    orderedMonthlySegmentTotal.get(monthMap.get(monthIntervalReversed[counter])).put(segment, segmentTotalMap.get(segment)[counter]);
                }
                counter++;
            }
        }
        return orderedMonthlySegmentTotal;
    }
    
    public static Map<String, Set<String>> getAccountSegment(){
        Map<String, Set<String>> accountSegmentMap = new Map<String, Set<String>>();
        
        //Retrieve account product custom metadata type 
        for(Account_Product__mdt meta: SCB_QueryWithoutSharing.getAccountProduct()){
            //Group products by segment type (level 1)
            if(!accountSegmentMap.containsKey(meta.Level_1__c)){
                accountSegmentMap.put(meta.Level_1__c, new Set<String>{meta.Financial_Account_Record_Type_API_Name__c});
            } else {
                accountSegmentMap.get(meta.Level_1__c).add(meta.Financial_Account_Record_Type_API_Name__c);
            }
        }
        return accountSegmentMap;
    }
    
    public class DataSet {
        public DataSet(String label, Decimal count){
            this.label = label;
            this.count = count;
        }

        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled
        public Decimal count {get;set;}
    }
}