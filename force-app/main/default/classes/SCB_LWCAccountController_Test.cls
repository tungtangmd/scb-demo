@isTest
public class SCB_LWCAccountController_Test {

    static testMethod void testSearchCustomer(){
        List<Account> accountList = SCB_TestDataFactory.createPersonAccounts(1);
        Account a = accountList.get(0);
        a.ID_No__pc = '12345';
        insert a;

        Account b = new Account();
        Account c = new Account();

        SCB_LWCAccountController.SObjectWrapper objWrap = new SCB_LWCAccountController.SObjectWrapper();
        Test.startTest();
            objWrap = SCB_LWCAccountController.getClient(a.Id);
            c = SCB_LWCAccountController.searchCustomer('12345', 'ID_No__pc');
        Test.stopTest();

        System.assertEquals(a.Id, objWrap.accRec.Id);
        System.assertEquals(a.Id, c.Id);
    }

    static testMethod void testGetClient(){
        List<Account> accountList = SCB_TestDataFactory.createPersonAccounts(1);
        Account a = accountList.get(0);
        insert a;

        SCB_LWCAccountController.SObjectWrapper objWrap = new SCB_LWCAccountController.SObjectWrapper();
        Boolean result = false;
        Test.startTest();
            objWrap = SCB_LWCAccountController.getClient(a.Id);
            result = objWrap.hasRecordAccess;
        Test.stopTest();

        System.assertEquals(a.Id, objWrap.accRec.Id);
        System.assertEquals(true, result);
    }

    static testMethod void PopUpView(){
        List<Account> accountList = SCB_TestDataFactory.createPersonAccounts(1);
        Account a = accountList.get(0);
        insert a;

        Opportunity opp = SCB_TestDataFactory.createOpportunity(Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Onshore ETB').getRecordTypeId(), a.Id);
		insert opp;

        SCB_LWCAccountController.SObjectWrapper objWrap = new SCB_LWCAccountController.SObjectWrapper();
        Test.startTest();
            objWrap = SCB_LWCAccountController.getRecordView(opp.Id, 'Opportunity', 'TestRecordType');
        	SCB_LWCAccountController.getFieldOptions(); // this is for displaying list option only
        Test.stopTest();

        System.assert(!objWrap.customView.isEmpty());
    }
}