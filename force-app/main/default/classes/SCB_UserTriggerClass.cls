public with sharing class SCB_UserTriggerClass {
    public static boolean isBeforeInsertExecuted = false;
    public static boolean isBeforeUpdateExecuted = false;
    public static boolean isBeforeDeleteExecuted = false;
    public static boolean isAfterInsertExecuted = false;
    public static boolean isAfterUpdateExecuted = false;
    public static boolean isAfterDeleteExecuted = false;
    public static boolean isAfterUndeleteExecuted = false;
    
    // public static void clear_execution_flags(){
    // isBeforeInsertExecuted = false;
    // isBeforeUpdateExecuted = false;
    // isBeforeDeleteExecuted = false;
    // isAfterInsertExecuted = false;
    // isAfterUpdateExecuted = false;
    // isAfterDeleteExecuted = false;
    // isAfterUndeleteExecuted = false;
    // }
    
    /* Method name: executeTriggerEvents
     * Purpose: This method checks the trigger events and accordingly execute the 
     *          methods  along with the trigger context variables and data
     */
    public static void executeTriggerEvents(Boolean isBefore, Boolean isAfter, 
                                            Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, 
                                            Map<Id, User> oldMap, List<User> newList, Map<Id, User> newMap){
        // if(isBefore){
        //     if(isInsert){
        //        if(!isBeforeInsertExecuted){
		// 			isBeforeInsertExecuted = true;
        //        }
        //     }
            
        //     if(isUpdate){
        //        if(!isBeforeUpdateExecuted){
        //             isBeforeUpdateExecuted = true;
        //        }
        //     }
            
        //     if(isDelete){
        //        if(!isBeforeDeleteExecuted){
        //            isBeforeDeleteExecuted = true;
        //        }
        //     }
        // }
        
        if(isAfter){
            if(isInsert){
			    if(!isAfterInsertExecuted){
                    updateBMGroup(newList, new Map<Id, User>(), new Map<Id, User>());
                    isAfterInsertExecuted = true;
                }
            }
            
            if(isUpdate){
			    if(!isAfterUpdateExecuted){
                    updateBMGroup(new List<User>(), oldMap, newMap);
                    isAfterUpdateExecuted = true;
                }
            }
            
            // if(isDelete){
            //     if(!isAfterDeleteExecuted){
            //         isAfterDeleteExecuted = true;
            //     }
            // }
            
            // if(isUndelete){
            //     if(!isAfterUndeleteExecuted){
            //         isAfterUndeleteExecuted = true;
            //     }
            // }
        }
    }

    public static void updateBMGroup(List<User> newList, Map<Id, User> oldMap, Map<Id, User> newMap){
        Profile bmProfile = [SELECT Id FROM Profile WHERE Name =: Label.SCB_Affluent_Sales_BM_Profile LIMIT 1];
        Set<Id> userIdSetAdd = new Set<Id>();
        Set<Id> userIdSetRemove = new Set<Id>();

        for(User u: newList){
            if(u.ProfileId == bmProfile.Id){
                userIdSetAdd.add(u.Id);
            }
        }
        for(User u: newMap.values()){
            if((oldMap.get(u.Id).ProfileId != u.ProfileId  || 
                !oldMap.get(u.Id).isActive  && u.isActive) && 
               u.ProfileId == bmProfile.Id){
                userIdSetAdd.add(u.Id);
            
            } else if((oldMap.get(u.Id).ProfileId != u.ProfileId || 
                       oldMap.get(u.Id).isActive  && !u.isActive) && 
                      oldMap.get(u.Id).ProfileId == bmProfile.Id){
                
                userIdSetRemove.add(u.Id);
            }
        }

        if(!userIdSetAdd.isEmpty() || 
           !userIdSetRemove.isEmpty()){
            Group bmGroup = [SELECT Id FROM GROUP WHERE DeveloperName =: SCB_Constants.PUBLIC_GROUP_AFFLUENT_SALES_BM_GROUP LIMIT 1];

            if(!userIdSetAdd.isEmpty()){
                List<GroupMember> groupMemberListAdd = new List<GroupMember>(); 
                Set<Id> userIdSetExisting = new Set<Id>();
                for(GroupMember groupMember: [SELECT UserOrGroupId FROM GroupMember WHERE GroupId =: bmGroup.Id AND UserOrGroupId IN: userIdSetAdd]){
                    userIdSetExisting.add(groupMember.UserOrGroupId);
                }

                for(Id userId: userIdSetAdd){
                    if(!userIdSetExisting.contains(userId)){
                        GroupMember groupMem = new GroupMember(); 
                        groupMem.GroupId = bmGroup.Id;
                        groupMem.UserOrGroupId = userId;
                        groupMemberListAdd.add(groupMem);
                    }
                }
                
                if(!groupMemberListAdd.isEmpty()){
                    List<Database.SaveResult> srList = Database.insert(groupMemberListAdd, false);
                    SCB_ApexErrorLogUtility.logDMLResult('SCB_UserTrigger', 'SCB_UserTriggerClass', 'updateBMGroup', srList);
                }
            }
    
            if(!userIdSetRemove.isEmpty()){
                List<GroupMember> groupMemberListRemove = [SELECT Id FROM GroupMember WHERE GroupId =: bmGroup.Id AND UserOrGroupId IN: userIdSetRemove];
                if(!groupMemberListRemove.isEmpty()){
                    List<Database.DeleteResult> srList = Database.delete(groupMemberListRemove, false);
                    SCB_ApexErrorLogUtility.logDMLResult('SCB_UserTrigger', 'SCB_UserTriggerClass', 'updateBMGroup', srList);
                }
            }
        }
    }
}