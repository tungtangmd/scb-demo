@isTest
private class SCB_AccountTriggerClass_Test {
    @testSetup static void staticRecords(){

    }
    
    @isTest static void insertAccount_Test() {
        List<Account> accList = SCB_TestDataFactory.createPersonAccounts(10);

        Test.StartTest();
        insert accList;
        Test.StopTest();
        
        Map<Id, String> accountMap = new Map<Id, String>();
        for(Account insertedAccount: [SELECT PersonContactId, Relationship_No__c FROM Account]){
            accountMap.put(insertedAccount.PersonContactId, insertedAccount.Relationship_No__c);
        }

        for(Contact updatedCon: [SELECT Id, Relationship_No__c FROM Contact WHERE Id IN: accountMap.keySet()]){
            System.assertEquals(accountMap.get(updatedCon.Id), updatedCon.Relationship_No__c);
        }
    }

    @isTest static void updateAccount_Test() {
        List<Account> accList = SCB_TestDataFactory.createPersonAccounts(10);
        insert accList;

        Integer counter = 0;
        for(Account acc: accList){
            acc.Relationship_No__c = 'abc'+counter;
            counter++;
        }

        Test.StartTest();
        update accList;
        Test.StopTest();
        
        Map<Id, String> accountMap = new Map<Id, String>();
        for(Account insertedAccount: [SELECT PersonContactId, Relationship_No__c FROM Account]){
            accountMap.put(insertedAccount.PersonContactId, insertedAccount.Relationship_No__c);
        }

        for(Contact updatedCon: [SELECT Id, Relationship_No__c FROM Contact WHERE Id IN: accountMap.keySet()]){
            System.assertEquals(accountMap.get(updatedCon.Id), updatedCon.Relationship_No__c);
        }
    }
}