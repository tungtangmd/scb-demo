public with sharing class SCB_Constants {
    
    public static final String SEGMENT_TYPE_WEALTH_MANAGEMENT = 'Wealth Management';
    public static final String SEGMENT_TYPE_DEPOSITS = 'Deposits';
    public static final String SEGMENT_TYPE_BANCA = 'Banca';
    public static final String SEGMENT_TYPE_LOANS = 'Loans';

    public static final String ACCOUNT_TYPE_INVESTMENT = 'Investment';
    public static final String ACCOUNT_TYPE_PCI = 'PCI';
    public static final String ACCOUNT_TYPE_STRUCTURED_DEPOSIT = 'Structured Deposit';
    public static final String ACCOUNT_TYPE_CURRENT_ACCOUNT = 'Current Account';
    public static final String ACCOUNT_TYPE_SAVINGS_ACCOUNT = 'Savings Account';
    public static final String ACCOUNT_TYPE_TIME_DEPOSIT = 'Time Deposit';
    public static final String ACCOUNT_TYPE_OVERDRAFT = 'Overdraft';
    public static final String ACCOUNT_TYPE_MORTGAGE = 'Mortgage';
    public static final String ACCOUNT_TYPE_PERSONAL_LOAN = 'Personal Loan';
    public static final String ACCOUNT_TYPE_CREDIT_CARD = 'Credit Card';

    public static final String FINANCIAL_ACCOUNT_STATUS_ACTIVE = 'Active';
    
    public static final String PUBLIC_GROUP_AFFLUENT_SALES_BM_GROUP = 'Affluent_Sales_BM_Group';
}