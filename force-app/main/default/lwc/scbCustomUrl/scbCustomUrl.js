import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getRecordView from '@salesforce/apex/SCB_LWCAccountController.getRecordView';

export default class ScbCustomUrl extends LightningElement {
    @api label;
    @api urlLink;
    @api isAccessible;
    @api recordId;
    @api objectName;
    @api recordType;
    openModal = false;
    records;

    _restrictAccess = false;
    @api set restrictAccess(value){
        this._restrictAccess = value;
    }
    get restrictAccess(){
        return this._restrictAccess;
    }
    clicked() {
        if(this.restrictAccess){
            const evt = new ShowToastEvent({
                title: "Error",
                message: `You don't have permission to view this record`,
                variant: "error",
            });
            this.dispatchEvent(evt);
        } else {
            this.openModal = true;
            if(!this.records){
                getRecordView({recordId : this.recordId, objectName : this.objectName, recordType : this.recordType})
                .then(data => {
                    this.records = [];
                    data.customView.forEach(field => {
                        this.records.push(field);
                    })
                })
                .catch(error => {
                    console.log(`error ${JSON.stringify(error)}`);
                });
            }
        }
    }
    closeModal(){
        this.openModal = false;
    }
}