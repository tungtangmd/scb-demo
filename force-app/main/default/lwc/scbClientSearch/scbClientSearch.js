import { LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import searchCustomer from '@salesforce/apex/SCB_LWCAccountController.searchCustomer';
import getFieldOptions from '@salesforce/apex/SCB_LWCAccountController.getFieldOptions';

export default class ScbClientSearch extends LightningElement {
    
    clientData;
    searchField; // default to ID No.
    fieldValue
    options;
    optionMap = new Map();

    // get selections
    connectedCallback() {
        getFieldOptions()
            .then(data => {
                this.options = [];
                data.forEach(field => {

                    if(field.IsDefault__c){
                        this.searchField = field.FieldAPIName__c
                    }
                    const temp = {
                        label: field.MasterLabel,
                        value: field.FieldAPIName__c
                    }
                    this.options.push(temp);
                    this.optionMap.set(field.FieldAPIName__c, field.MasterLabel);
                });
            })
    }
    keywordchange(event){
        this.fieldValue = event.detail.value;
    }

    fieldChange(event) {
        this.searchField = event.detail.value;
    } 

    searchClient(){
        this.clientData = undefined;
        if(this.fieldValue){
            searchCustomer({fieldValue : this.fieldValue, fieldName : this.searchField})
            .then(data => {
                this.clientData = data;
            })
            .catch(error => {
                const evt = new ShowToastEvent({
                    title: "Not Found",
                    message: `No record with ${this.optionMap.get(this.searchField)} ${this.fieldValue} was found. Please enter the exact keyword and search again.`,
                    variant: "error",
                });
                this.dispatchEvent(evt);
            });
        } else {
            const evt = new ShowToastEvent({
                title: "Warning",
                message: `Please populate all the fields and search again.`,
                variant: "warning",
            });
            this.dispatchEvent(evt);
        }
    }
}