import { LightningElement, api, wire, track } from 'lwc';
import getClient from '@salesforce/apex/SCB_LWCAccountController.getClient';

export default class ScbClient360 extends LightningElement {
    @api recordId;
    @api clientDataParam;
    @track clientData;

    connectedCallback(){
        if(this.recordId){
            getClient({recId : this.recordId}) 
            .then(data => {
                this.clientData = data.accRec;
            })
            .catch(error => {
                this.clientData = undefined;
            });
        } else if(this.clientDataParam){
            this.recordId = this.clientDataParam.Id;
            this.clientData = this.clientDataParam;
        }
    }
}