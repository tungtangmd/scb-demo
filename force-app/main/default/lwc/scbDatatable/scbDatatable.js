import LightningDatatable from 'lightning/datatable';
import customurl from './customurl.html';
export default class scbDatatable extends LightningDatatable {
    static customTypes = {
        customurl: {
            template: customurl,
            typeAttributes: ['urlLink','isAccessible','restrictAccess','objectName','recordId','recordType'],
        }
        //more custom types here
    };
}