import { LightningElement, api } from 'lwc';
import getClient from '@salesforce/apex/SCB_LWCAccountController.getClient';

export default class ScbActionsSection extends LightningElement {
    @api recordId;
    @api accountData;
    @api customActions;

    accountId;
    contactId;
    customActions = [{ label: 'Custom action', name: 'custom_action' }];

    connectedCallback(){

        //If this component is brought from Lightning App Builder only recordId is defined. Therefore we should query tor etrieve accountData as well
        if( (this.accountData == undefined) && (this.recordId !== undefined)){
            getClient({recId : this.recordId}) 
            .then(data => {
                this.accountData = data.accRec;
            })
            .catch(error => {
                this.accountData = undefined;
            });
        }
        if(this.accountData !== undefined)
        {
            this.accountId = this.recordId;
        }
    }
}