import { LightningElement, api, track } from 'lwc';

export default class ScbRelatedListOpportunity extends LightningElement {
    @api recordId;
    defaultValues;

    connectedCallback(){ 
        this.defaultValues = {AccountId: this.recordId}
    }

    renderedCallback(){
        this.accountId = this.recordId
    }

    @track opptyColumns = [
        {
            label: 'Name',
            fieldName: 'Name',
            type: 'customurl',
            typeAttributes: {
                urlLink: {
                    fieldName : 'LinkName'
                },
                isAccessible: {
                    fieldName : 'Id_isVisible'
                },
                objectName: 'Opportunity',
                recordId: {
                    fieldName : 'Id'
                },
                recordType: {
                    fieldName : 'RecordType_DeveloperName'
                }
            }
        },
        { label: 'Stage', fieldName: 'StageName', type: 'text' },
        { label: 'Amount', fieldName: 'Amount', type: 'currency', cellAttributes: { alignment: 'left' } },
        { label: 'Close Date', fieldName: 'CloseDate', type:"date-local", typeAttributes:{month:"2-digit", day:"2-digit"} }
    ]

    accountId
    customActions = [{ label: 'Custom action', name: 'custom_action' }]

}