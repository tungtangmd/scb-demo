import { LightningElement, api, track } from 'lwc';
import getClient from '@salesforce/apex/SCB_LWCAccountController.getClient';

import { createMessageContext, releaseMessageContext, subscribe, unsubscribe } from 'lightning/messageService';
import ServiceComplaintLMS from "@salesforce/messageChannel/ServiceComplaintLMS__c";

export default class ScbClientInfoSection extends LightningElement {
    @api recordId;
    @api accountdata;
    @track lastModId;
    @track createdId;

    hasservicecomplaint = false;
    
    context = createMessageContext();

    connectedCallback() {
        //if we drag and drop this component from Lightning App Builder, only recordId will be filled. This is why we need to query to fill AccountData as well
        if( (this.accountdata == undefined) && (this.recordId !== undefined)){
            getClient({recId : this.recordId}) 
            .then(data => {
                this.accountdata = data.accRec;
            })
            .catch(error => {
                this.accountdata = undefined;
            });
        }
        if(this.accountdata !== undefined)
        {
            this.lastModId = '/' + this.accountdata.LastModifiedById;
            this.createdId = '/' + this.accountdata.CreatedById; 
        }

        // subscribe to lightning messages
        subscribe(this.context, ServiceComplaintLMS, (message) => {
            this.handleMessage(message);
        });
    }
 
    handleMessage(message) {
        if(message.servicecomplaintfound)
            this.hasservicecomplaint = true;
    }
 
    disconnectedCallback() {
        releaseMessageContext(this.context);
    }
}